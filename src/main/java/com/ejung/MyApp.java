package com.ejung;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class MyApp {

    @ResponseBody
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String upload(@RequestParam("file") MultipartFile multipartFile) {
        System.out.println("### uploaded : {}" + multipartFile.getOriginalFilename());
      File targetFile = new File("/tmp/3scale-file-receiver-" + multipartFile.getOriginalFilename());
      try {
        InputStream fileStream = multipartFile.getInputStream();
        FileUtils.copyInputStreamToFile(fileStream, targetFile);
      } catch (IOException e) {
        FileUtils.deleteQuietly(targetFile);
        System.out.println("Fail to save the uploaded file.");
      }
      return "OK";
    }

    @RequestMapping(value = "/uploadMulti", method = RequestMethod.POST)
    public String uploadMulti(@RequestParam("file") List<MultipartFile> multipartFiles) {
    	for (MultipartFile multipartFile : multipartFiles) {
    	      File targetFile = new File("/tmp/3scale-file-receiver-" + multipartFile.getOriginalFilename());
    	      try {
    	        InputStream fileStream = multipartFile.getInputStream();
    	        FileUtils.copyInputStreamToFile(fileStream, targetFile);
    	      } catch (IOException e) {
    	        FileUtils.deleteQuietly(targetFile);
    	        System.out.println("Fail to save the uploaded file.");
    	      }

    	}
      return "OK";
    }
    
    @RequestMapping("/requestHeader")
    @ResponseBody
    public Message displayMessage(@RequestHeader(value="Custom-Request") String customRequest) {
        System.out.println("customRequest: " + customRequest);
        return new Message();
    }

    @RequestMapping("/responseHeader")
    @ResponseBody
    public ResponseEntity<String> displayHeaderMessage(@RequestHeader(value="Custom-Request") String customRequest) {

        System.out.println("customRequest: " + customRequest);
        SimpleDateFormat format1 = new SimpleDateFormat ( "yyyy-MM-dd HH:mm:ss");

        HttpHeaders headers = new HttpHeaders();
        headers.add("custom-Response-Time", format1.format(new Date()));
        return ResponseEntity.ok()
                           .headers(headers)
                           .body("SUCCESS");
    }

/*     @RequestMapping("/")
    @ResponseBody
    public Message displayMessage() {
        return new Message();
    } */

/*     @RequestMapping("/")
    public String index() {
       //has to be without blank spaces
       return "forward:index.html";
    }     */
    static class Message {
        private String content = "Greetings!";

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
}